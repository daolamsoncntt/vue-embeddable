require('./bootstrap');

window.Vue = require('vue');

Vue.component('mywork', require('./components/Mywork.vue'));

const app = new Vue({
    el: '#app'
});
